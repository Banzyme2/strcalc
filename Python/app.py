"""
    This is a string calculater module
    It takes arbitrary length string numbers
    and calculate their sum
"""

def compute_sum(*args):
    numbers = convert_to_int(*args)
    numbers_sum = add_numbers(numbers)
    return numbers_sum


def add_numbers(number_list):
    total = 0
    for num in number_list:
        # Remove numbers greater than 1000 or negative
        if(num>100):
            continue

        if(num < 0):
            raise ValueError('Negative number {0} not allowed!'.format(num));
        total = total + num
    return total


def convert_to_int(*args):
    num_list = []
    arg_list = []
    for arg in args:
        for item in arg.split(','):
            num_list.append(int( item.strip().replace('\n','') ))
    return num_list

if __name__ == "__main__":
    compute_sum()
