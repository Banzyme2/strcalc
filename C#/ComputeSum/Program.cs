﻿using System;
using System.Collections.Generic;
using static StringCalculator.utils;

namespace StringCalculator {
    public class Program {
        public const string testString = "//[*][%]\n1*2%3";
        public const int FIRST_DELIMETER_INDEX = 3;
        static void Main (string[] args) {
            Console.WriteLine ("This app prints out the sum of string numbers e.g. '1,2' returns sum = 3");
            Console.WriteLine(findStringNumbersSum (testString, FIRST_DELIMETER_INDEX));
        }

        public static int findStringNumbersSum (string Numbers, int delimeter_index=3) {
            int[] numList = new int[] { };

            if ( Numbers.Contains ("[") || Numbers.Contains ("]")) {
                string[] stringNumbers = new string[] { };
                string[] delimeterList = delimeter_list (Numbers, delimeter_index);

                stringNumbers = Numbers.Split (delimeterList[0]);
                // // Remove leading characters before fist number
                var strNumsCopy = new List<string> (stringNumbers);
                strNumsCopy.RemoveAt (0);
                stringNumbers = strNumsCopy.ToArray ();

                stringNumbers[0] = stringNumbers[0].Split ('\n') [1];

                numList = utils.detect_numbers (stringNumbers, delimeterList);
                return arraySum(numList);
            } else {
                numList = utils.convert_to_int (Numbers);
                return arraySum(numList);
            }
            // For debugging:
            // foreach (int i in numList) {
            //     Console.Write ("Detected numbers {0}: is {1}, \n", i, i);
            // }
            // Console.WriteLine ("");
            // Console.WriteLine ("Computed sum is: {0}", arraySum (numList));
        }

    }
}